# window_sill_extension
This file describes a component that is mounted on a cable duct in front 
of the window sill in our laboratory to create space for smaller experimental 
set-ups.
